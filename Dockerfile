FROM node:alpine as build

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm i

FROM node:alpine

WORKDIR /usr/src/app
COPY --from=build /usr/src/app/node_modules ./node_modules

COPY . .

CMD ["npm", "start"]