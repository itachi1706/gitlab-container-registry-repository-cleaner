import Gitbeaker from "@gitbeaker/node";
import dotenv from "dotenv";

const {ProjectsBundle} = Gitbeaker;
dotenv.config();

// Check environments present

const gitlabToken = process.env.GITLAB_TOKEN;
const projectID = process.env.GITLAB_PROJECT;

if (!gitlabToken) {
    console.error("No GitLab Token (GITLAB_TOKEN) found in env path!");
    process.exit(1);
}

if (!projectID) {
    console.error("No Project ID (GITLAB_PROJECT) found in env path!");
    process.exit(1);
}

const services = new ProjectsBundle({token: gitlabToken});

async function processCode() {
    console.log("Obtaining list of container registry repositories");
    let d = await services.ContainerRegistry.repositories(projectID, {tags_count: true});
    //console.log(d);
    let reposToRemove = [];
    for (let repo of d) {
        if (repo.tags_count <= 0) {
            reposToRemove.push({id: repo.id, path: repo.path});
        }
    }
    console.log("The following repositories are empty and will be removed shortly");
    let keyArray = reposToRemove.map(function(item) { return item.path; });
    console.log(keyArray);

    for (let repo of reposToRemove) {
        await services.ContainerRegistry.removeRepository(projectID, repo.id);
        console.log(`Repository ${repo.path} removed successfully!`)
    }
}

processCode();

