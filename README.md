GitLab.com Container Registry Repository Cleaner
---

Just a basic script that crawls through and delete GitLab Container Registries Repositories that are empty from a single project 



## How to use
### via .gitlab-ci.yml
Make sure you define a personal access token with `api` scope in your project's CI/CD Variables Settings
Then run the following in the gitlab-ci script of the repository
```yaml
cleanup:
  image: registry.gitlab.com/itachi1706/gitlab-container-registry-repository-cleaner:latest
  variables:
    GITLAB_TOKEN: $GITLAB_PAT # Replace with name of CI/CD Variable
    GITLAB_PROJECT: $CI_PROJECT_ID
    GIT_STRATEGY: none
  before_script:
    - cd /usr/src/app
  script: npm start
```

### Manually via Docker
Replace `<token>` and `<Project ID>` with your [GitLab Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) with the `api` scope and your Project's ID respectively. The Project ID can be accessed under Settings>General>Project ID in your GitLab repository. 
```bash
docker run --rm -e GITLAB_TOKEN=<token> GITLAB_PROJECT=<Project ID> registry.gitlab.com/itachi1706/gitlab-container-registry-repository-cleaner:latest
```